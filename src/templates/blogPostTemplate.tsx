import React from 'react'
import { graphql, Link } from 'gatsby'
import Post from '@shared/blog/Post/Post'
import Head from '@shared/Layout/Head'
import { IGatsbyImageData } from 'gatsby-plugin-image'
import BlogLayout from '@sections/Blog/components/BlogLayout'
import FixedHeader from '@shared/FixedHeader'
import styled from 'styled-components'
import { Post as PostType } from 'domain/types'

const LayoutContent = styled.div`
  padding: 20vh 0;

  h2 {
    font-size: 3em;
  }
`

const MoreArticles = styled.div`
  padding-top: 2em;
`

interface Props {
  data: {
    post: {
      frontmatter: {
        title: string
        description: string
        tags: string[]
        date: string
        path: string
      }
      html: string
    }
    site: {
      siteMetadata: {
        title: string
      }
    }
    smallLogo: {
      childImageSharp: {
        gatsbyImageData: IGatsbyImageData
      }
    }
  }
}

const Template: React.FC<Props> = (props: Props) => {
  const data = props.data
  const post: PostType = {
    ...data.post.frontmatter,
    date: new Date(data.post.frontmatter.date),
    content: data.post.html,
  }

  return (
    <>
      <Head
        title={data.post.frontmatter.title}
        description={data.post.frontmatter.description}
        path={data.post.frontmatter.path}
        type="article"
      />
      <FixedHeader
        title={data.site.siteMetadata.title}
        logo={data.smallLogo.childImageSharp.gatsbyImageData}
        path={['blog']}
      />
      <BlogLayout>
        <LayoutContent>
          <Post post={post} />

          <MoreArticles>
            <Link
              to="/blog"
              data-umami-event="click"
              data-umami-event-section="blog"
              data-umami-event-action="read-other-posts"
            >
              Read other posts in the blog
            </Link>
          </MoreArticles>
        </LayoutContent>
      </BlogLayout>
    </>
  )
}

export default Template

export const pageQuery = graphql`
  query ($path: String!) {
    post: markdownRemark(frontmatter: { path: { eq: $path } }) {
      frontmatter {
        title
        description
        tags
        date
        path
      }
      html
    }
    site {
      siteMetadata {
        title
      }
    }
    smallLogo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        gatsbyImageData(width: 25, height: 25, quality: 100, layout: FIXED)
      }
    }
  }
`
