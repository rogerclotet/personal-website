import { IGatsbyImageData } from 'gatsby-plugin-image'

export interface Post {
  title: string
  description: string
  path: string
  date: Date
  tags: string[]
  content: string
}

export interface Job {
  company: string
  companyLink: string
  title: string
  logo: IGatsbyImageData
  dates: string
  html: string
}

export interface Project {
  title: string
  image: IGatsbyImageData
  link: string
  repo?: string
  date: Date
  html: string
}
