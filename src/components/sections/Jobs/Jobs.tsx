import React from 'react'
import './Jobs.scss'
import RevealingContainer from '../../shared/RevealingContainer'
import { Job } from 'domain/types'
import styled from 'styled-components'
import { GatsbyImage } from 'gatsby-plugin-image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilePdf } from '@fortawesome/free-solid-svg-icons'

const CompanyTitle = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  > .gatsby-image-wrapper {
    width: 40px;
    margin-right: 0.5em;
  }
`

const ResumeLink = styled.a`
  display: flex;
  align-items: center;

  > *:first-child {
    padding-right: 0.5em;
  }
`

interface Props {
  jobs: Job[]
}

const Jobs: React.FC<Props> = (props: Props) => {
  return (
    <div className="jobs">
      <RevealingContainer>
        <div className="jobs-heading">
          <h1>Work Experience</h1>
          <ResumeLink
            href="https://gitlab.com/rogerclotet/resume/-/raw/master/resume.pdf"
            target="_blank"
            rel="noopener noreferrer"
            data-umami-event="click"
            data-umami-event-section="jobs"
            data-umami-event-action="resume"
          >
            <div>
              <FontAwesomeIcon icon={faFilePdf} />
            </div>
            Resume
          </ResumeLink>
        </div>

        {props.jobs.map((job, i) => (
          <div key={i} className="job">
            <RevealingContainer>
              <h3>
                <a
                  href={job.companyLink}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <CompanyTitle>
                    <GatsbyImage image={job.logo} alt={`${job.company} Logo`} />
                    <span>{job.company}</span>
                  </CompanyTitle>
                </a>
              </h3>
              <h2>{job.title}</h2>
              <p className="dates">{job.dates}</p>
              <div dangerouslySetInnerHTML={{ __html: job.html }} />
            </RevealingContainer>
          </div>
        ))}
      </RevealingContainer>
    </div>
  )
}

export default Jobs
