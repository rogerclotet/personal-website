const path = require('path')

module.exports = {
  siteMetadata: {
    title: `Roger Clotet Solé`,
    description: `I'm a software craftsman based in Girona. I build stuff for the web and distributed systems. I love learning, photography, videogames, and driving.`,
    author: 'Roger Clotet Solé',
    authorHandle: `@clotet_dev`,
    siteUrl: `https://clotet.dev`,
  },
  trailingSlash: 'never',
  plugins: [
    `gatsby-plugin-typescript`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
        path: path.join(__dirname, 'src', 'content'),
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: path.join(__dirname, 'src', 'images'),
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        sassOptions: {
          data: '@import "main.scss";',
          includePaths: ['src/'],
        },
        postCssPlugins: [
          require(`postcss-preset-env`)({ stage: 0 }),
          require('tailwindcss'),
          require('autoprefixer'),
        ],
      },
    },
    {
      resolve: `gatsby-plugin-umami`,
      options: {
        websiteId: 'a2c71f33-7bcc-426f-8a58-28017c99933c',
        srcUrl: 'https://apollo.clotet.dev/script.js',
        includeInDevelopment: false,
        autoTrack: true,
        respectDoNotTrack: true,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-prismjs`,
            options: {
              noInlineHighlight: false,
              prompt: {
                user: 'roger',
                host: 'localhost',
                global: false,
              },
            },
          },
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 640,
            },
          },
          {
            resolve: 'gatsby-remark-external-links',
            options: {
              target: '_blank',
              rel: 'noopener noreferrer',
            },
          },
        ],
      },
    },
    `gatsby-plugin-remove-serviceworker`,
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allMarkdownRemark } }) => {
              return allMarkdownRemark.edges.map(edge => ({
                title: edge.node.frontmatter.title,
                description: edge.node.frontmatter.description,
                date: edge.node.frontmatter.date,
                url: site.siteMetadata.siteUrl + edge.node.frontmatter.path,
                guid: site.siteMetadata.siteUrl + edge.node.frontmatter.path,
                custom_elements: [{ 'content:encoded': edge.node.html }],
              }))
            },
            query: `
              {
                allMarkdownRemark(
                  filter: { fileAbsolutePath: { regex: "/blog/" } }
                  sort: { frontmatter: { date: ASC } }
                ) {
                  edges {
                    node {
                      frontmatter {
                        title
                        description
                        path
                        date
                      }
                      html
                    }
                  }
                }
              }
            `,
            output: '/rss.xml',
            title: 'Dev Learnings - Roger Clotet Solé',
            description: 'My thoughts and notes about (mostly) web development',
          },
        ],
      },
    },
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-fontawesome-css`,
    `gatsby-plugin-use-query-params-v2`,
  ],
}
