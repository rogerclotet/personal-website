# My personal website

[![Netlify Status](https://api.netlify.com/api/v1/badges/1a09c14d-dcc0-4e67-852d-b862aa68a719/deploy-status)](https://app.netlify.com/sites/competent-wright-fcc061/deploys)

## Development

```bash
yarn install
yarn start
```
