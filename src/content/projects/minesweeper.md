---
title: Project Minesweeper
image: ./minesweeper.png
link: https://minesweeper.clotet.dev
repo: https://gitlab.com/rogerclotet/minesweeper
date: 2021-04-16
---

This is a multiplayer minesweeper game I built to learn about Socket.IO and NodeJS, inspired by Minesweeper Flags.

It uses real-time communication with server state persistence. It has a chat, editable user profiles,
configurable web and desktop notifications, and a game list with board preview to manage multiple concurrent games.

On the technical side, I used ReactJS, the previously mentioned Socket.IO, MaterialUI, tailwindcss, and MongoDB.

You can read more about it in the blog: 
[Project Minesweeper Part 1: Introduction and Tech](/blog/project-minesweeper-1-introduction-tech)
