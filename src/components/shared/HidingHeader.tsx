import React, { useEffect, useState } from 'react'
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image'
import { debounce } from 'lodash'
import { animateScroll } from 'react-scroll/modules'
import styled from 'styled-components'
import { color } from '@constants/colors'

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1;
  padding: 0.8em 1em;

  display: flex;
  justify-content: space-between;
  align-items: center;

  transition: all 0.2s ease-in;

  @media (max-width: 1400px) {
    background-color: ${color.background};
  }

  &.invisible {
    opacity: 0;
    transform: translateY(-1em);
  }
`

const Title = styled.h3`
  color: ${color.white};
  font-size: 1.3em;
`

interface Props {
  title: string
  logo: IGatsbyImageData
}

const HidingHeader: React.FC<Props> = (props: Props) => {
  const [isRendered, setIsRendered] = useState(false)
  const [isVisible, setIsVisible] = useState(false)

  const handleScroll = () => {
    const displayAt = window.innerHeight / 2
    const distanceFromTop = window.scrollY

    const shouldBeVisible = distanceFromTop >= displayAt

    if (isVisible !== shouldBeVisible) {
      if (shouldBeVisible) {
        setIsRendered(true)
      }
      setIsVisible(shouldBeVisible)
    }
  }

  useEffect(() => {
    const debouncedScroll = debounce(handleScroll, 50)

    window.addEventListener('scroll', debouncedScroll)

    return () => {
      window.removeEventListener('scroll', debouncedScroll)
    }
  }, [isVisible])

  useEffect(() => {
    if (!isVisible) {
      const timeoutId = setTimeout(() => setIsRendered(false), 200)
      return () => clearTimeout(timeoutId)
    }
  }, [isVisible])

  const scrollToTop = async () => {
    setIsVisible(false)
    animateScroll.scrollToTop()
  }

  return (
    <Container className={!isVisible ? 'invisible' : ''}>
      {isRendered && (
        <>
          <a href="#" onClick={scrollToTop}>
            <GatsbyImage image={props.logo} alt="Logo" />
          </a>
          <Title>{props.title}</Title>
        </>
      )}
    </Container>
  )
}

export default HidingHeader
