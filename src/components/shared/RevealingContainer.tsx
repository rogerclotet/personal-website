import React, { useEffect, useRef } from 'react'

interface Props {
  children: React.ReactNode
}

const ClientRevealingContainer: React.FC<Props> = ({ children }: Props) => {
  const container = useRef<HTMLDivElement>(null)

  useEffect(() => {
    if (typeof window !== 'undefined') {
      import('scrollreveal').then(
        ScrollReveal =>
          container.current &&
          ScrollReveal.default({ distance: '1em' }).reveal(container.current)
      )
    }
  }, [])

  return (
    <div ref={container} className="load-hidden">
      {children}
    </div>
  )
}

export default ClientRevealingContainer
