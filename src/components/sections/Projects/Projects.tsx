import React from 'react'
import { Project } from 'domain/types'
import styled from 'styled-components'
import { GatsbyImage } from 'gatsby-plugin-image'
import { color } from '@constants/colors'
import RevealingContainer from '@shared/RevealingContainer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGitlab } from '@fortawesome/free-brands-svg-icons'

const Container = styled.div`
  padding: 20vh 0 2em;
`

const SubTitle = styled.p`
  font-size: 1.1em;
`

const ProjectWrapper = styled.div`
  padding: 4em 0;
  @media (min-width: 580px) {
    padding: 4em;
  }

  &:not(:last-child) {
    border-bottom: 1px solid ${color.primary};
  }
`

const Centered = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`

interface Props {
  projects: Project[]
}

const Projects: React.FC<Props> = (props: Props) => {
  return (
    <RevealingContainer>
      <Container>
        <h1>Personal projects</h1>
        <SubTitle>
          {`These are some of my personal projects. Most things I develop as 
          side projects don't end up anywhere and only serve as learning 
          experiences, but these are the ones I think are worth sharing the most:`}
        </SubTitle>

        {props.projects.map((project, i) => (
          <ProjectWrapper key={i}>
            <RevealingContainer>
              <h2>
                <a
                  href={project.link}
                  target="_blank"
                  rel="noopener noreferrer"
                  data-umami-event="click"
                  data-umami-event-section="projects"
                  data-umami-event-action="project"
                  data-umami-event-project={project.title
                    .toLowerCase()
                    .split(' ')
                    .join('_')}
                >
                  {project.title}
                </a>{' '}
                - {project.date.getFullYear()}
              </h2>
              <div dangerouslySetInnerHTML={{ __html: project.html }} />
              {project.repo && (
                <p>
                  <a
                    href={project.repo}
                    target="_blank"
                    rel="noopener noreferrer"
                    data-umami-event="click"
                    data-umami-event-section="projects"
                    data-umami-event-action="source-code"
                    data-umami-event-project={project.title
                      .toLowerCase()
                      .split(' ')
                      .join('_')}
                  >
                    <FontAwesomeIcon icon={faGitlab} /> Source code
                  </a>
                </p>
              )}
              <Centered>
              <GatsbyImage image={project.image} alt={project.title} />
              </Centered>
            </RevealingContainer>
          </ProjectWrapper>
        ))}
      </Container>
    </RevealingContainer>
  )
}

export default Projects
