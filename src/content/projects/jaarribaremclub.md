---
title: Ja Arribarem Club website
image: ./jaarribaremclub.png
repo: https://gitlab.com/rogerclotet/jaarribaremclub
date: 2006-03-01
---

A website for a popular walk event I've built, iterated upon, and maintained since 2006.

It started as a static site to provide basic information of the current year's event, and since then I've rewritten it a
couple of times to what it is today: s Symfony application with an administration zone to edit and add new contents
every year and require minimal maintenance from my side.
