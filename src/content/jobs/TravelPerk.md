---
company: "TravelPerk"
companyLink: "https://travelperk.com"
title: "Senior Software Engineer"
logo: ./TravelPerk.png
dates: "June 2019 - March 2020, November 2021 - April 2024"
order: 4
---

I worked in TravelPerk as a senior full-stack software engineer, using React for frontend and Python with Django and Tornado for backend.

One of my significant contributions was a large effort transitioning the frontend codebase from JavaScript with Flow typing to TypeScript, enhancing maintainability and uncovering latent bugs.

I also contributed to shared backend projects, significantly improving developer experience and code quality. I actively participated in backend and frontend guilds to promote best practices among developers.

I was also involved in hiring and onboarding new people to the team, improving and refining processes and interviews.

I left to improve my work-life balance and rejoined when remote work became an option.
