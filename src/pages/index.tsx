import React from 'react'
import Layout from '@shared/Layout/Layout'
import Head from '@shared/Layout/Head'
import Intro from '@sections/Intro/Intro'
import Jobs from '@sections/Jobs/Jobs'
import { graphql } from 'gatsby'
import { IGatsbyImageData } from 'gatsby-plugin-image'
import HidingHeader from '@shared/HidingHeader'
import Contact from '@sections/Contact/Contact'
import BlogPreview from '@sections/Blog/BlogPreview'
import { Job, Post, Project } from 'domain/types'
import Projects from '@sections/Projects/Projects'

interface PostEdge {
  node: {
    frontmatter: {
      title: string
      description: string
      path: string
      tags: string[]
      date: string
    }
    html: string
  }
}

interface JobEdge {
  node: {
    frontmatter: {
      company: string
      companyLink: string
      title: string
      dates: string
      logo: {
        childImageSharp: {
          gatsbyImageData: IGatsbyImageData
        }
      }
    }
    html: string
  }
}

interface ProjectEdge {
  node: {
    frontmatter: {
      title: string
      image: {
        childImageSharp: {
          gatsbyImageData: IGatsbyImageData
        }
      }
      link: string
      repo?: string
      date: string
    }
    html: string
  }
}

interface Props {
  data: {
    site: {
      siteMetadata: {
        title: string
        description: string
      }
    }
    blog: {
      edges: PostEdge[]
    }
    jobs: {
      edges: JobEdge[]
    }
    projects: {
      edges: ProjectEdge[]
    }
    logo: {
      childImageSharp: {
        gatsbyImageData: IGatsbyImageData
      }
    }
    smallLogo: {
      childImageSharp: {
        gatsbyImageData: IGatsbyImageData
      }
    }
  }
}

const IndexPage: React.FC<Props> = (props: Props) => {
  const jobs: Job[] = props.data.jobs.edges.map(edge => ({
    ...edge.node.frontmatter,
    logo: edge.node.frontmatter.logo?.childImageSharp?.gatsbyImageData,
    html: edge.node.html,
  }))

  const posts: Post[] = props.data.blog.edges.map(edge => ({
    ...edge.node.frontmatter,
    date: new Date(edge.node.frontmatter.date),
    content: edge.node.html,
  }))

  const projects: Project[] = props.data.projects.edges.map(edge => ({
    ...edge.node.frontmatter,
    image: edge.node.frontmatter.image.childImageSharp.gatsbyImageData,
    date: new Date(edge.node.frontmatter.date),
    html: edge.node.html,
  }))

  return (
    <>
      <Head
        title={props.data.site.siteMetadata.title}
        description={props.data.site.siteMetadata.description}
        path=""
        type="website"
      />
      <HidingHeader
        title={props.data.site.siteMetadata.title}
        logo={props.data.smallLogo.childImageSharp.gatsbyImageData}
      />
      <Layout>
        <Intro
          title={props.data.site.siteMetadata.title}
          logo={props.data.logo.childImageSharp.gatsbyImageData}
        />
        <BlogPreview posts={posts} />
        <Jobs jobs={jobs} />
        <Projects projects={projects} />
        <Contact />
      </Layout>
    </>
  )
}

export default IndexPage

export const pageQuery = graphql`
  query Index {
    site {
      siteMetadata {
        title
        description
      }
    }
    logo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        gatsbyImageData(width: 50, height: 50, quality: 100, layout: FIXED)
      }
    }
    smallLogo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        gatsbyImageData(width: 25, height: 25, quality: 100, layout: FIXED)
      }
    }
    blog: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/blog/" } }
      sort: { frontmatter: { date: DESC } }
      limit: 3
    ) {
      edges {
        node {
          frontmatter {
            title
            description
            path
            tags
            date
          }
          html
        }
      }
    }
    jobs: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/jobs/" } }
      sort: { frontmatter: { order: DESC } }
    ) {
      edges {
        node {
          frontmatter {
            company
            companyLink
            title
            logo {
              childImageSharp {
                gatsbyImageData(
                  width: 40
                  height: 40
                  quality: 90
                  layout: FIXED
                )
              }
            }
            dates
          }
          html
        }
      }
    }
    projects: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/projects/" } }
      sort: { frontmatter: { date: DESC } }
    ) {
      edges {
        node {
          frontmatter {
            title
            image {
              childImageSharp {
                gatsbyImageData(quality: 90, layout: CONSTRAINED)
              }
            }
            link
            repo
            date
          }
          html
        }
      }
    }
  }
`
