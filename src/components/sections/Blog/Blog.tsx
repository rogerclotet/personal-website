import React from 'react'
import styled from 'styled-components'
import { color } from '@constants/colors'
import { Link } from 'gatsby'
import { Post } from 'domain/types'
import Tag from '@sections/Blog/components/Tag'

const PostList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  padding: 1em 0;
  margin: 0 -1em -1em 0;
`

const PostPreview = styled.li`
  list-style: none;

  padding: 1em 1.5em;
  margin: 0 1em 1em 0;
  border-radius: 0.5em;

  background-color: ${color.backgroundDark};

  @media (min-width: 580px) {
    max-width: calc(50% - 1em);
  }
`

const PostTitle = styled.h2`
  font-size: 1.2em;

  a {
    color: ${color.primary};
    font-weight: normal;

    transition: color 0.3s;

    &:hover {
      color: ${color.active};
    }
  }
`

const TagList = styled.ul`
  display: flex;
  flex-wrap: wrap;
  font-size: 0.9em;
  gap: 0.5em;
`

interface Props {
  posts: Post[]
}

const Blog: React.FC<Props> = (props: Props) => {
  return (
    <PostList>
      {props.posts.map(post => (
        <PostPreview key={post.path}>
          <PostTitle>
            <Link to={post.path}>{post.title}</Link>
          </PostTitle>
          <p>{post.description}</p>
          {post.tags && (
            <TagList>
              {post.tags.map(tag => (
                <Tag key={tag} tag={tag} link />
              ))}
            </TagList>
          )}
        </PostPreview>
      ))}
    </PostList>
  )
}

export default Blog
