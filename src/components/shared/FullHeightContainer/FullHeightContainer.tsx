import React from 'react'
import './FullHeightContainer.scss'

interface Props {
  children: React.ReactNode
}

const FullHeightContainer: React.FC<Props> = ({ children }: Props) => {
  return <div className="full-height-container">{children}</div>
}

export default FullHeightContainer
