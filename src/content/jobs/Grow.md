---
company: "Grow: The Design Consultancy"
companyLink: "https://grow-tdc.com"
title: "Web Developer"
logo: ./Grow.png
dates: "June - September 2010"
order: 1
---

I improved the company’s website as well as developed websites for several clients using PHP and the Silex framework
with Symfony components.
