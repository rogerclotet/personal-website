import React from 'react'
import FullHeightContainer from '@shared/FullHeightContainer/FullHeightContainer'
import RevealingContainer from '@shared/RevealingContainer'
import styled from 'styled-components'
import { color } from '@constants/colors'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEnvelope } from '@fortawesome/free-solid-svg-icons'

const Container = styled.div`
  display: flex;
  align-items: center;
`

const LeftPart = styled.div`
  flex-grow: 1;
  margin-right: 1em;

  > *:first-child {
    border-bottom: 1px solid ${color.primary};
  }
`

const Email = styled.a`
  display: flex;
  align-items: center;

  > *:first-child {
    padding-right: 0.5em;
  }
`

const Contact: React.FC = () => {
  return (
    <FullHeightContainer>
      <RevealingContainer>
        <h1>{"That's it!"}</h1>
        <Container>
          <LeftPart>
            <div>
              <p>
                {
                  "I'm always working in side projects and willing to collaborate."
                }
              </p>
            </div>
            <div>
              <p>Feel free to get in touch!</p>
            </div>
          </LeftPart>
          <div>
            <Email
              href="mailto:roger@clotet.dev"
              data-umami-event="click"
              data-umami-event-section="contact"
              data-umami-event-action="email"
            >
              <div>
                <FontAwesomeIcon icon={faEnvelope} />
              </div>
              roger@clotet.dev
            </Email>
          </div>
        </Container>
      </RevealingContainer>
    </FullHeightContainer>
  )
}

export default Contact
