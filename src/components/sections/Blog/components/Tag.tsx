import React from 'react'
import styled from 'styled-components'
import { color } from '@constants/colors'
import { Link } from 'gatsby'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

const ListItem = styled.li`
  list-style: none;

  display: inline-flex;
  align-items: center;
  gap: 0.3em;

  background-color: ${color.secondary};
  color: ${color.primaryDarker};

  border-radius: 1em;
  padding: 0 0.4em;
`

interface Props {
  tag: string
  link?: boolean
  onDelete?: () => void
}

const Tag: React.FC<Props> = ({ tag, link, onDelete }) => {
  const item = (
    <ListItem>
      <span>{tag}</span>
      {onDelete && (
        <span onClick={onDelete} className="cursor-pointer">
          <FontAwesomeIcon icon={faTimes} />
        </span>
      )}
    </ListItem>
  )

  if (link) {
    return (
      <Link
        to={`/blog?tag=${tag}`}
        data-umami-event="click"
        data-umami-event-section="blog"
        data-umami-event-action="tag"
        data-umami-event-tag={tag}
      >
        {item}
      </Link>
    )
  }

  return item
}

export default Tag
