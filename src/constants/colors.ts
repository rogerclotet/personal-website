export const color = {
  background: 'rgba(47, 52, 55, 1)',
  backgroundDark: 'rgba(35, 38, 41, 1)',
  white: 'rgba(252, 252, 252, 1)',
  grey: 'rgba(252, 252, 252, 0.6)',
  primary: 'rgba(61, 195, 126, 1)',
  primaryDark: 'rgba(53, 143, 95, 1)',
  primaryDarker: 'rgba(43, 99, 69, 1)',
  secondary: 'rgba(225, 244, 156, 1)',
  active: 'rgba(246, 151, 177, 1)',
}
