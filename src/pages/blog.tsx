import React from 'react'
import { graphql } from 'gatsby'
import Head from '@shared/Layout/Head'
import { IGatsbyImageData } from 'gatsby-plugin-image'
import Blog from '@sections/Blog/Blog'
import styled from 'styled-components'
import { color } from '@constants/colors'
import BlogLayout from '@sections/Blog/components/BlogLayout'
import FixedHeader from '@shared/FixedHeader'
import { Post } from 'domain/types'
import { useQueryParam, StringParam } from 'use-query-params'
import Tag from '@sections/Blog/components/Tag'

const Title = styled.h1`
  padding-top: 20vh;
`

const Subtitle = styled.h5`
  color: ${color.white};
`

interface PostEdge {
  node: {
    frontmatter: {
      title: string
      description: string
      path: string
      tags: string[]
      date: string
    }
    html: string
  }
}

interface Props {
  data: {
    site: {
      siteMetadata: {
        title: string
        description: string
      }
    }
    smallLogo: {
      childImageSharp: {
        gatsbyImageData: IGatsbyImageData
      }
    }
    blog: {
      edges: PostEdge[]
    }
  }
}

const BlogPage: React.FC<Props> = (props: Props) => {
  const [tag, setTag] = useQueryParam('tag', StringParam)

  let posts: Post[] = props.data.blog.edges.map(edge => ({
    ...edge.node.frontmatter,
    date: new Date(edge.node.frontmatter.date),
    content: edge.node.html,
  }))

  if (tag) {
    posts = posts.filter(post => post.tags.includes(tag))
  }

  const handleDeleteFilter = () => {
    setTag(undefined)
  }

  return (
    <>
      <Head
        title={
          tag
            ? `Dev Learnings about ${tag} - Roger Clotet Solé`
            : 'Dev Learnings - Roger Clotet Solé'
        }
        description="My thoughts and notes about software development"
        path="/blog"
        type="website"
      />
      <FixedHeader
        title={props.data.site.siteMetadata.title}
        logo={props.data.smallLogo.childImageSharp.gatsbyImageData}
        path={[]}
      />
      <BlogLayout>
        <Title>Dev Learnings</Title>
        <Subtitle>My thoughts and notes about software development</Subtitle>
        {tag && (
          <p>
            Filter: <Tag tag={tag} onDelete={handleDeleteFilter} />
          </p>
        )}
        <Blog posts={posts} />
      </BlogLayout>
    </>
  )
}

export default BlogPage

export const pageQuery = graphql`
  query Blog {
    site {
      siteMetadata {
        title
        description
      }
    }
    smallLogo: file(relativePath: { eq: "logo.png" }) {
      childImageSharp {
        gatsbyImageData(width: 25, height: 25, quality: 100, layout: FIXED)
      }
    }
    blog: allMarkdownRemark(
      filter: { fileAbsolutePath: { regex: "/blog/" } }
      sort: { frontmatter: { date: DESC } }
    ) {
      edges {
        node {
          frontmatter {
            title
            description
            path
            tags
            date
          }
          html
        }
      }
    }
  }
`
