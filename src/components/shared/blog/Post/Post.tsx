import React from 'react'
import { Post as PostType } from 'domain/types'
import './Post.scss'
import { Link } from 'gatsby'
import Tag from '@sections/Blog/components/Tag'

interface Props {
  post: PostType
  withTitleLink?: boolean
}

const Post: React.FC<Props> = ({ post, withTitleLink }: Props) => {
  return (
    <div className="post">
      <h2>
        {withTitleLink ? (
          <Link
            to={post.path}
            className={`color-primary umami--click--blog-title-${post.date}`}
            data-umami-event="click"
            data-umami-event-section="blog"
            data-umami-event-action="title"
            data-umami-event-title={post.title
              .toLowerCase()
              .split(' ')
              .join('_')}
          >
            {post.title}
          </Link>
        ) : (
          post.title
        )}
      </h2>
      <div className="subheader">
        <span className="date">{post.date.toLocaleDateString()}</span>
        {post.tags && (
          <div className="tags">
            {post.tags.map(tag => (
              <Tag key={tag} tag={tag} link />
            ))}
          </div>
        )}
      </div>
      <div
        dangerouslySetInnerHTML={{ __html: post.content }}
        className="content"
      />
    </div>
  )
}

export default Post
