import React from 'react'
import Layout from '@shared/Layout/Layout'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRssSquare } from '@fortawesome/free-solid-svg-icons'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`

const Content = styled.div`
  flex-grow: 1;
`

const Footer = styled.div`
  padding: 1em 0;
  text-align: center;

  a > *:first-child {
    margin-right: 0.5em;
  }
`

interface Props {
  children: React.ReactNode
}

const BlogLayout: React.FC<Props> = ({ children }: Props) => {
  return (
    <Layout>
      <Container>
        <Content>{children}</Content>
        <Footer>
          <a href="/rss.xml" target="_blank" rel="noopener noreferrer">
            <FontAwesomeIcon icon={faRssSquare} />
            RSS Feed
          </a>
        </Footer>
      </Container>
    </Layout>
  )
}

export default BlogLayout
