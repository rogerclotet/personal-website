import React from 'react'
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image'
import styled from 'styled-components'
import { color } from '@constants/colors'
import { Link } from 'gatsby'

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1;
  padding: 0.8em 1em;

  display: flex;
  justify-content: space-between;
  align-items: center;

  @media (max-width: 1400px) {
    background-color: ${color.background};
  }
`

const Breadcrumbs = styled.div`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  margin-right: 2em;
`

const Breadcrumb = styled.h5`
  margin-top: 0.3em;
  margin-left: 0.5em;
  white-space: nowrap;

  a {
    font-weight: normal;
    color: ${color.white};

    &:hover {
      color: ${color.active};
    }
  }
`

const Title = styled.h3`
  color: ${color.white};
  font-size: 1.3em;
  font-weight: normal;
  text-align: right;
`

interface Props {
  title: string
  logo: IGatsbyImageData
  path: string[]
}

const FixedHeader: React.FC<Props> = (props: Props) => (
  <Container>
    <Breadcrumbs>
      <Link to="/">
        <GatsbyImage image={props.logo} alt="Logo" />
      </Link>
      {props.path &&
        props.path.map((breadcrumb, i) => {
          return (
            <Breadcrumb key={i}>
              /{' '}
              <Link to={'/' + props.path.slice(i).join('/')}>{breadcrumb}</Link>
            </Breadcrumb>
          )
        })}
    </Breadcrumbs>
    <Link to="/">
      <Title>{props.title}</Title>
    </Link>
  </Container>
)

export default FixedHeader
