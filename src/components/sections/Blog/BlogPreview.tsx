import React from 'react'
import RevealingContainer from '@shared/RevealingContainer'
import Post from '@shared/blog/Post/Post'
import { Link } from 'gatsby'
import styled from 'styled-components'
import Blog from '@sections/Blog/Blog'
import { Post as PostType } from 'domain/types'

const Container = styled.div`
  padding: 20vh 0;
`

const PostPreview = styled.div`
  height: 55vh;
  min-height: 400px;
  overflow: hidden;
  mask-image: linear-gradient(to bottom, black 70%, transparent 100%);
  margin-bottom: 1em;
`

const ReadMore = styled.div`
  text-align: center;
`

interface Props {
  posts: PostType[]
}

const BlogPreview: React.FC<Props> = (props: Props) => {
  const {
    posts: [post, ...otherPosts],
  } = props

  return (
    <Container>
      <RevealingContainer>
        <h1>Latest posts</h1>

        <PostPreview>
          <Post key={post.path} post={post} withTitleLink />
        </PostPreview>

        <ReadMore>
          <Link
            to={post.path}
            className="button"
            data-umami-event="click"
            data-umami-event-section="blog"
            data-umami-event-action="continue"
          >
            Continue reading
          </Link>
        </ReadMore>
      </RevealingContainer>

      <RevealingContainer>
        <Blog posts={otherPosts} />
        <Link to="/blog" className="umami--click--blog-see-all-link">
          See all posts in the blog
        </Link>
      </RevealingContainer>
    </Container>
  )
}

export default BlogPreview
