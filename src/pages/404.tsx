import React from 'react'

import Layout from '@shared/Layout/Layout'
import Head from '@shared/Layout/Head'
import { graphql, Link } from 'gatsby'
import FullHeightContainer from '@shared/FullHeightContainer/FullHeightContainer'

interface Props {
  data: {
    site: {
      siteMetadata: {
        title: string
        description: string
      }
    }
  }
}

const NotFoundPage: React.FC<Props> = (props: Props) => (
  <Layout>
    <Head
      title={props.data.site.siteMetadata.title}
      description={props.data.site.siteMetadata.description}
      path=""
      type="website"
    />
    <FullHeightContainer>
      <div>
        <h1>Not found</h1>
        <p>This page doesn&#39;t exist.</p>
        <Link to="/" className="button">
          Get me to the homepage
        </Link>
      </div>
    </FullHeightContainer>
  </Layout>
)

export default NotFoundPage

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
        description
      }
    }
  }
`
