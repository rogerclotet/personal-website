import React from 'react'
import styled from 'styled-components'

const LayoutContainer = styled.div`
  display: flex;
  flex-direction: column;

  min-height: 100vh;

  max-width: 960px;
  margin: 0 auto;
  padding: 0 2em;

  @media (max-width: 768px) {
    padding: 0 1em;
  }
`

const Main = styled.main`
  flex-grow: 1;
`

interface Props {
  children: React.ReactNode
}

const Layout: React.FC<Props> = ({ children }: Props) => {
  return (
    <LayoutContainer>
      <Main>{children}</Main>
    </LayoutContainer>
  )
}

export default Layout
