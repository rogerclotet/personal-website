---
company: "Typeform"
companyLink: "https://typeform.com"
title: "Senior Software Engineer"
logo: ./Typeform.png
dates: "March 2020 - November 2021"
order: 3
---

I was the sole backend engineer in a team that was responsible for the form building aspect of the company.

My responsibilities included updating and maintaining several backend services, while also adding new features to enhance our offerings. Despite being primarily focused on backend development, I also contributed to parts of the frontend, ensuring a seamless user experience.

My efforts played a significant role in improving the developer experience and elevating the overall code quality.