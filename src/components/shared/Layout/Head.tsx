import React from 'react'
import { Helmet } from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'

interface Props {
  title: string
  description: string
  path: string
  type: 'website' | 'article'
}

const Head: React.FC<Props> = ({ title, description, path, type }: Props) => {
  const { site, favicon, logoWithBackground, logoWithBackgroundSquare } =
    useStaticQuery(
      graphql`
        query Site {
          site {
            siteMetadata {
              authorHandle
              siteUrl
            }
          }
          favicon: file(relativePath: { eq: "favicon.png" }) {
            publicURL
          }
          logoWithBackground: file(relativePath: { eq: "logo-rectangle.png" }) {
            publicURL
          }
          logoWithBackgroundSquare: file(
            relativePath: { eq: "logo-square.png" }
          ) {
            publicURL
          }
        }
      `
    )

  return (
    <Helmet
      htmlAttributes={{
        lang: 'en',
      }}
      title={title}
      titleTemplate={title}
      meta={[
        {
          name: 'description',
          content: description,
        },
        {
          property: 'og:url',
          content: site.siteMetadata.siteUrl + path,
        },
        {
          property: 'og:title',
          content: title,
        },
        {
          property: 'og:description',
          content: description,
        },
        {
          property: 'og:image',
          content: site.siteMetadata.siteUrl + logoWithBackground.publicURL,
        },
        {
          property: 'og:type',
          content: type,
        },
        {
          property: 'og:site_name',
          content: site.siteMetadata.title,
        },
        {
          property: 'twitter:card',
          content: 'summary',
        },
        {
          property: 'twitter:site',
          content: site.siteMetadata.authorHandle,
        },
        {
          property: 'twitter:creator',
          content: site.siteMetadata.authorHandle,
        },
        {
          property: 'twitter:image',
          content:
            site.siteMetadata.siteUrl + logoWithBackgroundSquare.publicURL,
        },
        {
          property: 'twitter:title',
          content: title,
        },
        {
          property: 'twitter:description',
          content: description,
        },
      ]}
      link={[
        {
          rel: 'icon',
          type: 'image/png',
          href: favicon.publicURL,
        },
        { rel: 'canonical', href: site.siteMetadata.siteUrl + path },
      ]}
      defer={false}
    />
  )
}

export default Head
