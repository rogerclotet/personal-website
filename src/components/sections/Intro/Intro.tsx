import React, { useEffect, useState } from 'react'
import FullHeightContainer from '@shared/FullHeightContainer/FullHeightContainer'
import { GatsbyImage, IGatsbyImageData } from 'gatsby-plugin-image'
import RevealingContainer from '@shared/RevealingContainer'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faGithub,
  faGitlab,
  faLinkedin,
  faMastodon,
} from '@fortawesome/free-brands-svg-icons'
import {
  faAngleDoubleDown,
  faEnvelope,
} from '@fortawesome/free-solid-svg-icons'
import './Intro.scss'

interface Props {
  title: string
  logo: IGatsbyImageData
}

const Intro: React.FC<Props> = (props: Props) => {
  const [hasScrolled, setHasScrolled] = useState(false)

  const handleScroll = () => {
    setHasScrolled(true)
    removeEventListener('scroll', handleScroll)
  }

  useEffect(() => {
    if (window.scrollY === 0) {
      window.addEventListener('scroll', handleScroll)
    } else {
      setHasScrolled(true)
    }
  }, [])

  return (
    <RevealingContainer>
      <FullHeightContainer>
        <div className="intro flex justify-between items-center flex-wrap">
          <div className="intro-main">
            <GatsbyImage image={props.logo} alt="Logo" />
            <h2 className="mt-4 mb-0 color-white">{props.title}</h2>
            <h1 className="whitespace-nowrap">Hello, World!</h1>
            <div className="icons color-white">
              <a
                href="https://gitlab.com/rogerclotet"
                target="_blank"
                rel="noopener noreferrer"
                data-umami-event="click"
                data-umami-event-section="intro"
                data-umami-event-action="gitlab"
              >
                <FontAwesomeIcon icon={faGitlab} />
                <span className="hidden">GitLab</span>
              </a>
              <a
                href="https://github.com/rogerclotet"
                target="_blank"
                rel="noopener noreferrer"
                data-umami-event="click"
                data-umami-event-section="intro"
                data-umami-event-action="github"
              >
                <FontAwesomeIcon icon={faGithub} />
                <span className="hidden">GitHub</span>
              </a>
              <a
                href="https://linkedin.com/in/rogerclotet"
                target="_blank"
                rel="noopener noreferrer"
                data-umami-event="click"
                data-umami-event-section="intro"
                data-umami-event-action="linkedin"
              >
                <FontAwesomeIcon icon={faLinkedin} />
                <span className="hidden">LinkedIn</span>
              </a>
              <a
                rel="me noopener noreferrer"
                href="https://mastodon.social/@clotet"
                target="_blank"
                data-umami-event="click"
                data-umami-event-section="intro"
                data-umami-event-action="mastodon"
              >
                <FontAwesomeIcon icon={faMastodon} />
                <span className="hidden">Mastodon</span>
              </a>
              <a
                href="mailto:roger@clotet.dev"
                data-umami-event="click"
                data-umami-event-section="intro"
                data-umami-event-action="email"
              >
                <FontAwesomeIcon icon={faEnvelope} />
                <span className="hidden">Email</span>
              </a>
            </div>
          </div>
          <div className="intro-text my-0 mx-auto text-right color-primary lg:pl-8">
            <div>
              {"I'm a dad and a software craftsman based in Girona"}
              <br />
              I build stuff for the web and distributed systems
              <br />I love learning, photography, videogames, and driving
            </div>
          </div>
        </div>
      </FullHeightContainer>
      {!hasScrolled && (
        <div className="arrow-scroll-down">
          <div>
            <FontAwesomeIcon icon={faAngleDoubleDown} />
          </div>
        </div>
      )}
    </RevealingContainer>
  )
}

export default Intro
